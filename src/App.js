import logo from "./logo.svg";
import "./App.css";
import TodoList from "./todoList/TodoList";

function App() {
  return (
    <div className="">
      <TodoList></TodoList>
    </div>
  );
}

export default App;
