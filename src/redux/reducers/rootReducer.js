import { combineReducers } from "redux";
import { reducerTodolist } from "./reducerTodolist";

export const rootReducer = combineReducers({
  reducerTodolist,
});
