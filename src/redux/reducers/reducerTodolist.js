import { userSerV } from "../../services/API/api.service";
import { todoListLocal } from "../../services/local.Service";
import { getApiService } from "../actions/todoListAction";
import { ADD_TASK, REMOVE_TASK } from "../constants/todoListConStants";

let initialState = {
  listTodo: [],
};
export const reducerTodolist = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SET":
      state.listTodo = payload;
      return { ...state };
    default:
      return state;
  }
};
