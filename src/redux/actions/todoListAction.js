import { API_service } from "../../services/API/api.service";
import { todoListLocal } from "../../services/local.Service";
import { ADD_TASK } from "../constants/todoListConStants";

export const getApiService = () => {
  return (dispatch) => {
    API_service.GET_API().then(function (res) {
      todoListLocal.set(res.data);
      dispatch({
        type: "SET",
        payload: res.data,
      });
    });
  };
};
export const postApiService = (payload) => {
  return (dispatch) => {
    API_service.POST_API(payload).then((res) => {
      dispatch(getApiService());
    });
  };
};
export const putApiService = (id) => {
  return (dispatch) => {
    let listTodo = todoListLocal.get();
    let index = listTodo.findIndex((item) => {
      return item.id == id;
    });
    let payload = {
      ...listTodo[index],
      check: listTodo[index].check == true ? false : true,
    };
    API_service.PUT_API(payload).then((res) => {
      dispatch(getApiService());
    });
  };
};
export const removeApiService = (id) => {
  return (dispatch) => {
    API_service.DEL_API(id).then(function (res) {
      dispatch(getApiService());
    });
  };
};
