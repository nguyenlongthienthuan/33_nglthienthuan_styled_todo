import React, { memo } from 'react'
import { useDispatch } from 'react-redux'
import styled from 'styled-components'
import { putApiService, removeApiService } from '../redux/actions/todoListAction'
const Li=styled.li`
display: flex;
justify-content: space-between;
width: 100%;
min-height: 30px;
font-size: 0.8rem;
font-weight: 500;
color: #444;
background: #fff;
border-radius: 5px;
box-shadow: 0px 0px 2px rgba(44, 62, 80, 0.3);
margin: 0 0 10px 0;
padding: 0 10px; 
word-break: break-word;
.buttons {
    button {
        background: none;
        border: 0px;
        box-shadow: none;
        outline: none;
        cursor: pointer;
        position: relative;
        padding: 5px 0 5px 5px;
        font-size: 1rem;
      }
      .remove{
        color: #aaa;
        &:hover{
            color: #FA396B;
          }
      }
      .complete{
        color: #aaa;
        &:hover{
            color: #25b99a;
          }
      }
      
}
`
 function ItemCompleted({name,id}) {
  let dispatch=useDispatch()
  let handelRemoveTask=(id)=>{
    dispatch(removeApiService(id))
  }
  let handelPutTask=(id)=>{
    dispatch(putApiService(id))
  }
  return (
   <Li><span>{name}</span> 
  <div className="buttons">
    <button onClick={()=>{handelRemoveTask(id)}} className="remove"><i className="fa fa-trash-alt" /></button> 
    <button  onClick={()=>{handelPutTask(id)}}  className="complete"><i className="fa fa-check-circle fas" /></button>
  </div>
</Li>
  )
}

export default memo(ItemCompleted)