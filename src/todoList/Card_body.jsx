import React from 'react'
import styled from 'styled-components'
import Card__content from './Card__content'
import Filter_btn from './Filter_btn'
const Card__body=styled.div`
position: relative;
&:before {
  content: '';
  position: absolute;
  top: -35px;
  left: -50px;
  background-color: #f5f8f9;
  height: 100px;
  width: 120%;
  display: block;
  transform: rotate(10deg);
}
`
export default function Card_body() {

  return (
<Card__body>
<Filter_btn></Filter_btn>
    <Card__content></Card__content>
</Card__body>
  )
}
