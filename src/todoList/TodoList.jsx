import React from 'react'
import styled from 'styled-components'
import Card_body from './Card_body'
import Card_header from './Card_header'
const Card=styled.div`
margin: 50px auto;
background-color: #f5f8f9;
/* height: 430px; */
width: 350px;
box-shadow: 0 0 6px grey;
overflow: hidden;
line-height: 2rem;
letter-spacing: 1px;
`
export default function TodoList() {
  return (
   <Card>
     <Card_header></Card_header>
     <Card_body></Card_body>
   </Card>

  )
}
