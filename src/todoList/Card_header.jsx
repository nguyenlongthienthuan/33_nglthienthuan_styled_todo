import React from 'react'
import styled from 'styled-components'
const Card__header=styled.div`
height: 215px;
  overflow: hidden;
  position: relative;
  &:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
   width: 100%;
   height: 100%;
    background: rgba(71, 32, 84, 0.2);
  }
`

export default function Card_header() {
  return (
   <Card__header>
    <img style={{width:`100%`}} src="./img/X2oObC4.png" />
   </Card__header>
  )
}
