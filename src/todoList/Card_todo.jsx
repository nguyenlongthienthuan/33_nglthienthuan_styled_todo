import React, { memo } from 'react'
import { useMemo } from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import ItemCompleted from './ItemCompleted'
import ItemTodo from './ItemTodo'
const Ul=styled.ul`
list-style: none; 
  &#todo{
    padding-top: 30px;
    &:empty:after {
        content: 'You have nothing to-do!';
        margin: 15px 0 0 0;
      }
      &:after {
        width: 100%;
        display: block;
        text-align: center;
        font-size: 12px;
        color: #aaa;
      }
      li{
        .buttons{
            .complete{
                 .fas{
                  display: none;
                 }
                 .far{
                  display: block;
                }
            }
        }
      }
  }
  &#completed {
    // background: red;
    position: relative;
    padding: 30px 0;
    &:before {
        content: '';
        width: 150px;
        height: 1px;
        background: #d8e5e0;
      
        position: absolute;
        top: 14px;
        left: 50%;
      
        margin: 0 0 0 -75px;
      }
      :empty:after {
        content: 'You have yet to complete any tasks.';
      }
      &:after {
        width: 100%;
        display: block;
        text-align: center;
        font-size: 12px;
        color: #aaa;
      }
      li{
        .buttons{
            .complete{
                 .fas{
                  display: block;
                  color: #25b99a;
                 }
                 .far{
                  display: none;
                }
            }
        }
      }
      span{
        color: #25b99a;
      }
  }
`
 function Card_todo({}) {
   
  let listTodo=useSelector((state)=>{
    return state.reducerTodolist.listTodo;
   })
    
    let renderListTodo=useMemo(()=>{
       console.log('list todo',listTodo);
       if (listTodo) {
        return listTodo.map((item)=>{if(item.check==false){ return <ItemTodo key={item.id} name={item.name} id={item.id}  ></ItemTodo>} })
       }
   },[[listTodo]])
    let renderListCompleted=useMemo(()=>{
      return listTodo.map((item)=>{if(item.check==true)return <ItemCompleted key={item.id} name={item.name} id={item.id}></ItemCompleted> })
   },[[listTodo]])
     
  console.log("card_todo");
  return (
    <div className="card__todo">
        {/* Uncompleted tasks */}
        <Ul id='todo' > 
          {renderListTodo}
        </Ul>
        {/* Completed tasks */}
        <Ul id="completed">
        {renderListCompleted}
        </Ul>
      </div>
  )
} 
export default memo(Card_todo)
