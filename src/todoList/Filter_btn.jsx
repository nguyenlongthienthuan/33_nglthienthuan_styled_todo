import React, { useState } from 'react'
import styled from 'styled-components'
const Filter__btn=styled.div`
    position: absolute;
    z-index: 2;
    right: 0;
    width: 40px;
    height: 40px;
    transition: all 0.4s ease-in-out 0s;
    a {
      position: absolute;
      width: 40px;
      height: 40px;
      line-height: 40px;
      text-align: center;
      right: 25px;
      display: block;
      top: -30px;
      color: #fff;
      z-index: 99;
      font-size: 1.2rem;
      transition: all .4s cubic-bezier(.68, 1, .265, 1)
    }
    span{
      i{
       width: 40px;
       height: 40px;
       background: #FA396B;
       display: block;
       position: absolute;
       right: 25px;
       top: -30px;
       text-align: center;
       color: #fff;
       line-height: 40px;
       border-radius: 50%;
       font-size: 1rem;
       z-index: 999;
        }
    .toggle-btn i{
        background-color: blue;
        }
    }
    &:after {
      height: 150px;
        width: 150px;
        content: '';
        background-color: #FA396B;
        position: absolute;
        top: -86px;
        right: -35px;
        border-radius: 50%;
        transform: scale(0);
        transition: all 0.3s ease-in-out 0s;
    }
    &.open{
        span{
            i{
                background-color: #DE3963;
            }
        }
        &:after {
            transform: scale(1);
          }
          .fa-times {
            display: block;
          }
          .fa-filter {
            display: none;
          }
          a{
            &:nth-child(1) {
                transform: translate(9px, -48px);
              } 
             &:nth-child(2) {
                transform: translate(-42px, -26px);
              } 
              &:nth-child(3) {
                transform: translate(-39px, 29px);
              }
              &:nth-child(4) {
                transform: translate(6px, 52px);
              }
          }
    }
    .fa-times {
        display: none;
      }
      .fa-filter {
        display: block;
      }
`

export default function Filter_btn() {
    let [test,setTest]=useState([]);
  return (
  <Filter__btn className='filter-btn'>
     <a id="one" href="#"><i className="fa fa-check-circle" /></a>
      <a id="two" href="#"><i className="fa fa-sort-alpha-down" /></a>
      <a id="three" href="#"><i className="fa fa-sort-alpha-up" /></a>
      <a id="all" href="#"><i className="fa fa-clock" /></a>
      <span onClick={()=>{setTest(test=test=true?false:true)}} className="toggle-btn">
        <i className="fa fa-filter" />
        <i className="fa fa-times" />
      </span>
  </Filter__btn>
  )
}
