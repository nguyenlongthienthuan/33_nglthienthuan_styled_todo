import React from 'react'
import styled from 'styled-components'
import Card_todo from './Card_todo'
import Card__add from './Card__add'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { getApiService } from '../redux/actions/todoListAction'
const Card_content=styled.div`
position: relative;
padding: 0 20px;
`
export default function Card__content() {
   let dispatch=useDispatch();
   useEffect(()=>{
          dispatch(getApiService())
   },[])
  return (
 <Card_content>
    <div className="card__title">
        <h2>My Tasks</h2>
        <p>September 9,2020</p>
      </div>
     <Card__add></Card__add>
     <Card_todo ></Card_todo>
 </Card_content>
  )
}
