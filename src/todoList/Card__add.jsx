import React, { memo } from 'react'
import { useState } from 'react'
import { useDispatch } from 'react-redux';
import styled from 'styled-components'
import { postApiService } from '../redux/actions/todoListAction';
const Card_add=styled.div`
display: flex;
position: relative;

margin-top: 5px;
input {
    width: 100%;
      height: 40px;
      /* float: left; */
      color: #fff;
      font-size: 15px;
      font-weight: 400;
      text-indent: 18px;
      /* padding: 0 60px 0 0; */
      background: rgba(222, 57, 99, 0.6);
      /* border-radius: 3px; */
      border-top-right-radius: 25px;
      border-bottom-right-radius: 25px;
      border: 0px;
      box-shadow: none;
      outline: none;
      &:placeholder{
        color: #fff;
      }
  } 
  button{
 
    border: none;
    border-radius: 50%;
    position: absolute;
    top:0;
    right: 0;
    /* width: 40px;
    height: 40px; */
    box-shadow: 0 0 0 2px rgba(222, 57, 99);
    cursor: pointer;
    outline: none;
  }
  i{
    width: 40px;
    height: 40px;
    border-radius: 25px;
    background: #fff;
    line-height: 40px;
    color: #DE3963;
  
  }
`
function Card__add() {
  let [task,setTask]= useState([]);
   let dispatch=useDispatch() 
   let get_task=(task)=>{
    
    let name=task
    let time= Date.now();
    let check=false;
   setTask(task=[])
    return {
        name:name,
        time:time,
        check:check,
    }
}
let handelGetTask=(e)=>{
  let value=e.target.value;
    setTask(task=value)
}
 let handelAddTask=(data)=>{
  
  dispatch(postApiService(data))
 }

  return (
    <Card_add>
        <input onChange={handelGetTask} id="newTask" type="text" placeholder="Enter an activity..."  value={task}/>
        <button onClick={()=>{handelAddTask(get_task(task))}} id="addItem">
          <i className="fa fa-plus" />
        </button>
    </Card_add> 
  )
}
export default memo(Card__add)
