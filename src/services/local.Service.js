let TODO_LIST = "TODO_LIST";
export const todoListLocal = {
  set: (data) => {
    console.log("savelocal");
    let json = JSON.stringify(data);
    sessionStorage.setItem(TODO_LIST, json);
  },
  get: () => {
    let json = sessionStorage.getItem(TODO_LIST);
    if (json) {
      return JSON.parse(json);
    } else {
      return [];
    }
  },
  remove: () => {
    localStorage.removeItem(TODO_LIST);
  },
};
