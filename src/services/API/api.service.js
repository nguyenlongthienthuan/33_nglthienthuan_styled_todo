import axios from "axios";
import { baseURL } from "./url.config";

export const API_service = {
  GET_API: () => {
    return axios({
      url: `${baseURL}/todo`,
      method: "GET",
    });
  },

  POST_API: (task) => {
    return axios({
      url: `${baseURL}/todo`,
      method: "POST",
      data: task,
    });
  },
  DEL_API: (id) => {
    return axios({
      url: `${baseURL}/todo/${id}`,
      method: "DELETE",
    });
  },
  PUT_API: (task) => {
    return axios({
      url: `${baseURL}/todo/${task.id}`,
      method: "PUT",
      data: task,
    });
  },
};
